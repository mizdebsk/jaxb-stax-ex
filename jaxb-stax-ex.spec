Name:           jaxb-stax-ex
Version:        2.1.0
Release:        1%{?dist}
Summary:        Extended StAX API
License:        BSD

URL:            https://github.com/eclipse-ee4j/jaxb-stax-ex
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  maven-local
BuildRequires:  mvn(jakarta.activation:jakarta.activation-api:2.1.0)
BuildRequires:  mvn(jakarta.xml.bind:jakarta.xml.bind-api:4.0.0)
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)

%description
This project contains a few extensions to complement JSR-173 StAX API in
the following areas:

- Enable parser instance reuse (which is important in the
  high-performance environment like Eclipse Implementation of JAXB and
  Eclipse Metro)
- Improve the support for reading from non-text XML infoset, such as
  FastInfoset.
- Improve the namespace support.

%{?javadoc_package}

%prep
%setup -q

find -name 'module-info.java' -type f -delete

%pom_remove_parent

%pom_remove_plugin :buildnumber-maven-plugin
%pom_remove_plugin :glassfish-copyright-maven-plugin
%pom_remove_plugin :maven-enforcer-plugin

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE.md NOTICE.md
%doc README.md

%changelog
* Tue Jan 17 2023 Marian Koncek <mkoncek@redhat.com> - 2.1.0-1
- Initial build
